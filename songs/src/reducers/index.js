import { combineReducers } from 'redux';

const songsReducer = () =>{
    return[
        {title: 'No scrubs', durations: '4:05'},
        {title: 'Macarena', durations: '3:05'},
        {title: 'All star', durations: '3:15'},
        {title: 'I want it in that way', durations: '1:45'},
    ];
};   

const selectedSongReducer = (selectedSong = null, action) =>{
    if(action.type === 'SONG_SELECTED'){
        return action.payload;
    }

    return selectedSong;
};

export default combineReducers({
    songs: songsReducer,
    selectedSong: selectedSongReducer
});