import React from 'react';
import { Link } from 'react-router-dom';
import GoogleOath from './GoogleOath'
const Header = () =>{
    return(
        <div className="ui secondary pointing menu">
            <Link to="/" className="item">
                Streamer
            </Link>
            <div className="right menu">
               <Link to="/" className="item">
                 All Streams
               </Link>
               <GoogleOath />
            </div>
        </div>
    );
};

export default Header;