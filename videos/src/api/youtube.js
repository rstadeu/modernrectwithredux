import axios from 'axios';
const KEY = 'AIzaSyAdD4Yz1bn_7wj_RFFa345UNeQKja-zaeI';

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params:{
        part: 'snippet',
        maxResults: 5,
        key: KEY
    }
});