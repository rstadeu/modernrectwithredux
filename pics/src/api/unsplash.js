import axios from 'axios';


export default axios.create({
    baseURL: 'https://api.unsplash.com',
    headers:{
        Authorization: 'Client-ID 22aa19bbcabbcbfb823bb355a23aba67edfb034e3f00f2eadaa53c18b4fa0347'
    }
});