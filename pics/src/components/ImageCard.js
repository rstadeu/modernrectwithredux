import React , { Component } from 'react';


class ImageCard extends Component{
    constructor(props){
        super(props)

        this.state = {spanss: 0};

        this.imageRef = React.createRef();
    }

    componentDidMount(){
        this.imageRef.current.addEventListener('load', this.setSpans);
    }

    setSpans = () =>{
        const height = this.imageRef.current.clientHeight;
        const spans = Math.ceil(height / 10);

        this.setState({ spanss: spans })
    }

    render(){
        return(
            <div style={{ gridRowEnd: `span ${this.state.spanss}` }}>
                <img
                    ref={this.imageRef}
                    alt={this.props.image.description}
                    src={this.props.image.urls.regular}
                    />
            </div>
        );
    }
};


export default ImageCard;